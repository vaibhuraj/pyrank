# This is written to demonstrate this language versus the same program written in other languages.
# 10-May-2020	Vaibhav	Created this.

# Create scoreboard file and declare variables.
scorefile="Scoreboard.txt"
guess=0
num=0

echo "Guess a number between 1 and 100"

### Generate random number

answer=`awk -v min=0 -v max=100 'BEGIN{srand(); print int(min+rand()*(max-min+1))}'`

### Play game

while [ $guess -ne $answer ]; do
	num=`expr $num + 1`
	echo "Enter guess $num: "
	read guess
	if [ $guess -lt $answer ]; then
		echo "Higher..."
	elif [ $guess -gt $answer ]; then
		echo "Lower..."
	fi
done
echo "Correct! That took $num guesses."

### Save high score

echo "Please enter your name: "
read name
echo $name $num >> $scorefile

### Print high scores

echo "Previous Scores"
echo "Previous Scores"
cat $scorefile
