import kaggle
import re
import pandas as pd
from sqlalchemy import create_engine
import pymysql
new_kaggle=kaggle.KaggleApi()
new_kaggle.authenticate()
dataset_location='tunguz/movietweetings'
dataset=new_kaggle.dataset_view(dataset_location)
dataset.files
#new_kaggle.dataset_download_cli(dataset_location,unzip=True)
movies_data = pd.read_csv('movies.dat',header=None, names=['movie_id', 'movie_title', 'genere'], sep='::', engine='python')
#print(movies_data)
#cnx = mysql.connector.connect(user='hive', password='hive',host='127.0.0.1',database='kaggle')
#movies_data.to_sql(movies_data, cnx, flavor='mysql', schema=None, if_exists='fail', index=True, index_label=None, chunksize=None, dtype=None)
#movies_data.to_sql('movies', con=db)
#db.execute("SELECT * FROM movies").fetchall()
#movies_data.to_sql('movies', cnx, index=False)
pymysql.install_as_MySQLdb()
engine = create_engine("mysql://hive:hive@localhost/kaggle")
con = engine.connect()
movies_data.to_sql(name='movies',con=con,if_exists='append',index=False)
con.close()