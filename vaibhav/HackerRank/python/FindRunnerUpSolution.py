## 1. create an input command to take n
## 2. create an input command to take an Array of size n
## 3. Sort the array
## 4. print the second highest value in the Array, i.e, print n-2

## Solution1 - Failed test cases involving negative numbers
n=int(input())
array=[int(i) for i in input().split()]
array.sort()
myset=set(array)
unique_list=(list(myset))
print(unique_list[-2])

## Solution 2 - Passed all test cases.
n = int(input())
arr = map(int, input().split())
print(sorted(list(set(arr)))[-2])

## 


