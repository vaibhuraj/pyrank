from sqlalchemy import create_engine
import pandas as pd
import pymysql
pymysql.install_as_MySQLdb()
engine = create_engine("mysql://hive:hive@localhost/kaggle")
con = engine.connect()
df = pd.DataFrame(['A','B'],columns=['new_tablecol'])
df.to_sql(name='new_table',con=con,if_exists='append')
con.close()