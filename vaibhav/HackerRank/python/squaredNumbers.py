# Task
# Read an integer N. For all non-negative integers i<N, print i^2. See the sample for details.
# Input Format
# The first and only line contains the integer, .
# Constraints
# Output Format
# Print  lines, one corresponding to each .

n=int(input())
i=0
while i<n:
    i+=1
    print(i**2)
    
