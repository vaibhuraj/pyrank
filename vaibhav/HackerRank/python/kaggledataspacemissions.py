import kaggle
import re
import pandas as pd
from sqlalchemy import create_engine
import pymysql
new_kaggle=kaggle.KaggleApi()
new_kaggle.authenticate()
dataset_location='agirlcoding/all-space-missions-from-1957'
dataset=new_kaggle.dataset_view(dataset_location)
dataset.files
#new_kaggle.dataset_download_cli(dataset_location,unzip=True)
space_mission_data = pd.read_csv('Space_Corrected.csv',header=None, names=['id1','id2','Company_Name','Location','Datum','Detail','Status_Rocket', 'Rocket','Status_Mission'], sep=',', engine='python')
#print(space_mission_data)
pymysql.install_as_MySQLdb()
engine = create_engine("mysql://hive:hive@localhost/kaggle")
con = engine.connect()
space_mission_data.to_sql(name='space_missions',con=con,if_exists='append',index=False)
con.close()